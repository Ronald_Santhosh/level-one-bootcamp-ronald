//WAP to find the sum of n fractions.

#include<stdio.h>
//A structure with 2 integer variables that store the Numerator and Denominator
struct fraction
{
	int num;
	int den;
};
int numbers()
//Function that accepts the number of fractions
{
	int n;
	printf("Enter the number of fractions :");
	scanf("%d",&n);	
	if( n < 0 )
	{
		printf("Enter a positive number instead \n");
		n = numbers();
	}
	return n;
}
int gcd_finder(int a, int b)
//Function that finds the gcd of two numbers and returns it
{
	if(b == 0)
	    return a;
	else
	    return gcd_finder(b,a%b);
}
struct fraction add(struct fraction arr[], int n)
//Function that adds the fractions and returns the sum.
{
	int gcd;
	struct fraction sum;
	sum.num = 0;
	sum.den = 1;
	for(int i=0;i<n;i++)
	{
	    sum.num = (arr[i].num*sum.den + sum.num*arr[i].den);
        	    sum.den = arr[i].den * sum.den; 
	    gcd = gcd_finder(sum.num, sum.den);
	    sum.num = sum.num/gcd;
	    sum.den = sum.den/gcd;
	}
	return sum;
}
void accept(struct fraction arr[],int n)
//Function that accepts an array of fractions 
{
	for(int i=0;i<n;i++)
	{
	    printf("Array number %d\n",(i+1));
	    printf("Enter Numerator :");
	    scanf("%d",&arr[i].num);
	    printf("Enter Denominator :");
	    scanf("%d",&arr[i].den);
	}
}
void display(struct fraction sum, struct fraction arr[], int n)
//Function that displays the sum of fractions
{
    if( n==0 )
    {
        printf("No fractions to add\nSum = 0\n");
    }
    else
    {
        printf("%d/%d ",arr[0].num,arr[0].den);
        for(int i=1;i<n;i++)
            printf("+ %d/%d ",arr[i].num,arr[i].den);
        printf(" = %d/%d",sum.num,sum.den);
    }
}

int main()
{
    int n = numbers(); //number of fractions
    struct fraction arr[n]; //array of fractions
    accept(arr,n); //accepts the fractions.
    struct fraction sum; //sum of fractions
    sum = add(arr,n); //calculates the sum.
    display(sum, arr, n); //displays the sum of fractions
    return 0;
}
