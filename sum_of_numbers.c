//Write a program to find the sum of n different numbers using 4 functions

#include<stdio.h>
//Function that adds the two numbers and returns the sum.
int add(int sum, int i)
{
    sum = sum + i; 
    return sum;
}
//Function that accepts a number and adds it to a variable sum 
int accept(int sum)
{
	int i=0;
	printf("Enter a number :");
	scanf("%d",&i);
	sum = add(sum,i);
	return sum;
}
int numbers()
//Function that accepts the value of n and returns it
{
	int n;
	printf("Enter how many numbers need to be entered :");
	scanf("%d",&n);
	if (n<0)
	{
	    printf("Enter a positive number instead\n");
	    n = numbers();
	}
	return n;
}
void display(int sum)
//Function that displays the sum of numbers as the output.
{
	printf("The sum of the given numbers is %d\n",sum);
}

void main()
{
	int n=0, sum =0;
	n = numbers();
	for(int j=1;j<=n;j++)
	{
	    sum = accept(sum);
	}
	display(sum);
}
