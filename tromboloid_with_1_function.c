//Write a program to find the volume of a tromboloid using one function

#include<stdio.h>

float tromboloid(float h,float d, float b);
int main()
{
    float h,d,b;
    //Accepting the input values h, d, and b
	printf("Enter h value:");
	scanf("%f",&h);
	printf("Enter d value:");
	scanf("%f",&d);
	printf("Enter b value:");
	scanf("%f",&b);
	//Using another function to calculate the volume
	float vol = tromboloid(h,d,b);
	printf("%f",vol);
	return 0;
}
float tromboloid(float h,float d, float b)
{
    return (1.0/3.0)*((h*d)+d)/b;
}
