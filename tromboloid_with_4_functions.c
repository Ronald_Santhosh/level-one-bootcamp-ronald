//WAP to find the volume of a tromboloid using 4 functions.

#include<stdio.h>
float accept_h();
float accept_d();
float accept_b();
void volume(float h, float d, float b);
void main()
{
	//Using 3 separate methods to accept the values h,d and b
	float h,d,b, vol;
	h = accept_h();
	d = accept_d();
	b = accept_b();
	//Using a separate method to calculate and display the volume
	volume(h,d,b);
}
float accept_h()
{
	float h;
	printf("Enter a number:");
	scanf("%f",&h);
	return h;
}
float accept_d()
{
	float d;
	printf("Enter a number:");
	scanf("%f",&d);
	return d;
}
float accept_b()
{
	float b;
	printf("Enter a number:");
	scanf("%f",&b);
	return b;
}
void volume(float h, float d, float b)
{
	float vol = (1.0/3.0)*((h*d)+d)/b;
//Volume of tromboloid is: 1/3 of ((h x d) + d) divided by b
	printf("The volume of the Tomboloid is %f",vol);
}
