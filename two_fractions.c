//WAP to find the sum of two fractions.

#include<stdio.h>
struct fraction
{
	int num;
	int den;
};
struct fraction accept()
{
	struct fraction f;
	printf("Enter the numerator :");
	scanf("%d",&f.num);
	printf("Enter the denominator :");
	scanf("%d",&f.den);
	return f;
}
int gcd_finder(int a, int b)
//Function that calculates the gcd and returns it.
{
	if(b == 0)
	    return a;
	else
	    return gcd_finder(b, a%b);
}
struct fraction add(struct fraction f1, struct fraction f2)
{
	struct fraction sum;
	sum.num = (f1.num*f2.den + f2.num*f1.den);
	sum.den = f1.den * f2.den;	
	int gcd = gcd_finder(sum.num, sum.den);
	sum.num = sum.num / gcd;
	sum.den = sum.den / gcd;
	return sum ;
}
void display(struct fraction sum,struct fraction f1,struct fraction f2)
{
	printf("%d/%d + %d/%d = %d/%d\n",f1.num,f1.den,f2.num,f2.den, sum.num, sum.den);
}
void main()
{
	struct fraction f1,f2,sum;
	f1 = accept();
	f2 = accept();
	sum = add(f1,f2);
	display(sum,f1,f2);
}
